task :sendToRails => :environment do
    desc "import data from files to database"
    # Get input from arguments
    hostname = ENV["HOSTNAME"]
    contact = ENV["CONTACT"]
    msg = ENV["MSG"]
    sev = ENV["SEV"]
    mobile = ENV["MOBILE"]
    #output correct information back to STDOUT
    puts "#{hostname} #{contact} #{msg} #{sev} #{mobile}\n"
    #Write variables to new record 
    m = Messages.new
    m.host = hostname
    m.contact = contact
    m.contents = msg
    m.severity = sev
    m.mobile = mobile
    m.status = "unsent"
    #save record to db
    m.save!
end

task :sendToAPN => :environment do
  desc "Send All Unsent to apple push notification"
  @messages = Messages.find_all_by_status("unsent")
  @messages.each do |message|
    notification = "#{message.host}-#{message.severity} -- #{message.contents}"
    @contact =  Contacts.find_all_by_name(message.contact)
    @contact.each do |contact|
      APNS.send_notification(contact.deviceToken, notification)
    end
    message.status = "unread"
    message.save!
  end
end
  
