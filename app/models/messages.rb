class Messages < ActiveRecord::Base
  attr_accessible  :host, :contact, :contents, :severity, :status, :mobile
end
