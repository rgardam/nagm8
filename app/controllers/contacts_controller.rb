class ContactsController < ApplicationController


  respond_to :json, :xml
    def show
     @contact = Contacts.find_by_id(params[:id])
    end

  def new
      @contact = Contacts.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @contact }
      end
    end

  def create
      @contact = Contacts.new(params[:contact])

      respond_to do |format|
        if @contact.save
          #format.html { redirect_to @person, notice: 'Person was successfully created.' }
          format.json { render json: @contact, status: :created, location: @contact }
        else
          format.html { render action: "new" }
          format.json { render json: @contact.errors, status: :unprocessable_entity }
        end
      end
  end


end
