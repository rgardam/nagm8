ActiveAdmin.register Contacts do

  member_action :sendmessage, :method => :get do
       contact = Contacts.find(params[:id])
       contact.sendmessage
       redirect_to :action => :show, :notice => "Test Message Sent"
     end

  action_item :only=> :show do
    link_to("Send Test Message", sendmessage_admin_contact_path)
  end


end
