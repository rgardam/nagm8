class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :host
      t.string :contact
      t.string :contents
      t.string :severity
      t.string :status

      t.timestamps
    end
  end
end
